<?php

if(!defined('BASEPATH'))
    die;

class Auth extends MY_Controller
{
    function __construct(){
        parent::__construct();
        
    }
    
    private function loginSet($user){
        $this->user = $user;
        
        $session = array(
            'user' => $user->id,
            'hash' => password_hash(time().'-'.$user->id, PASSWORD_DEFAULT)
        );
        
        $this->load->model('Usersession_model', 'USession');
        $this->USession->create($session);
        
        $cookie_name = config_item('sess_cookie_name');
        $cookie_expr = config_item('sess_expiration');
        $this->input->set_cookie($cookie_name, $session['hash'], $cookie_expr);
        $this->ajax($session);
    }
    
    public function login(){
        if($this->user)
            return $this->ajax('this_user');
        
        $this->load->library('SiteForm', '', 'form');
        $this->form->setForm('/admin/me/login');
        
        $params = array(
            'title' => _l('Login')
        );
        
        if(!($login=$this->form->validate()))
            return $this->ajax('failed', $params);
        
        $this->load->model('User_model', 'User');
        
        $name = $login['name'];
        $field = filter_var($name, FILTER_VALIDATE_EMAIL) ? 'email' : 'name';
        $user = $this->User->getBy($field, $name, 1);
        
        if(!$user){
            return $this->ajax('User not found with that identity', $params);
        }
        
        // banned or deleted user
        if($user->status < 2){
            return $this->ajax('User banned or deleted', $params);
        }
        
        if(!password_verify($login['password'], $user->password)){
            return $this->ajax('Invalid password', $params);
        }
        
        $this->event->me->logged_in($user);
        $this->loginSet($user);
        
    }
    
    public function logout(){


        $this->load->model('Usersession_model', 'USession');
        $token = $this->input->get('token');
        if(!$token)
            return $this->ajax('failed');
        
        $this->USession->removeByCond(['hash'=>$token]);
        $this->ajax('success_logout');
    }

}