<?php

if(!defined('BASEPATH'))
    die;

class User extends MY_Controller
{
    function __construct(){
        parent::__construct();
        
        $this->load->model('User_model', 'User');
        $this->load->library('ObjectFormatter', '', 'formatter');
    }
    
    public function single($identity){
        $user = null;
        if(is_numeric($identity))
            $user = $this->User->get($identity);
        
        if(!$user)
            $user = $this->User->getBy('name', $identity, 1);
        
        if(!$user)
            return $this->ajax(null, 'Not found');
        
        $user = $this->formatter->user($user, false, false);
        $this->ajax($user, false);
    }

    public function login(){
        $username = $this->input->post('username');
        //$password = $this->input->post('password');

        $hasil = $this->User->getByCond(['name'=>$username], false, false);
        $this->ajax($hasil, false);
    }
}