<?php
require APPPATH . '/libraries/REST_Controller.php';
/**
*
*/
class Post extends REST_Controller
{

    function __construct() {
        parent::__construct();
        header("Access-Control-Allow-Origin: *");

        $this->load->model('Post_model', 'Post');
        $this->load->library('ObjectFormatter', '', 'formatter');

    }

    function index_get() {
        $post = $this->Post->getByCond(['status'=>4], false, false);
        $format = $this->formatter->post($post);
        $posts = array();
        foreach ($format as $p) {
            $posts[] = array(
                'id' => $p->id,
                'title' => $p->title->chars(40),
                'cover' => $p->cover->_250x200,
                'gallery' => $p->gallery,
                'embed' => $p->embed,
                'published' => $p->published->format('d M Y')
                );
        }

        $this->response($posts, 200);
    }

    function filter_get() {
        $limit = 3;
        $page = $this->input->get('page');
        $post = $this->Post->findByCond(['status'=>4], $page, false);
        $format = $this->formatter->post($post);
        $posts = array();
        foreach ($format as $p) {
            $posts[] = array(
                'id' => $p->id,
                'title' => $p->title->chars(40),
                'cover' => $p->cover->_250x200,
                'gallery' => $p->gallery,
                'embed' => $p->embed,
                'published' => $p->published->format('d M Y')
                );
        }

        $this->response($posts, 200);
    }

    function featured_get() {
        $post = $this->Post->getBy('featured', 1);
        $format = $this->formatter->post($post);
        $feat = array(
            'id' => $format->id,
            'title' => $format->title->chars(50),
            'cover' => $format->cover->_430x300,
            'published' => $format->published->format('d M Y')
            );

        $this->response($feat, 200);
    }

    function read_get($id) {
        if(!$id)
            return false;

        $post = $this->Post->get($id);
        $format = $this->formatter->post($post);
        $read_p = array(
            'user' => $format->user->fullname,
            'title' => $format->title,
            'cover' => $format->cover->_430x300,
            'content' => $format->content,
            'page' => base_url($format->page),
            'published' => $format->published->format('d M Y')
            );

        $this->response($read_p, 200);
    }

    function category_get($id) {
        $post = $this->Post->findByCond(['status'=>4, 'category'=>$id], 12, false);
        $format = $this->formatter->post($post);
        $posts = array();
        foreach ($format as $p) {
            $posts[] = array(
                'id' => $p->id,
                'title' => $p->title->chars(50),
                'cover' => $p->cover->_250x200,
                'published' => $p->published->format('d M Y')
                );
        }

        $this->response($posts, 200);
    }

    function menu_get() {
        $this->load->model('Sitemenu_model', 'Menu');
        $menus = $this->Menu->getByCond(['group' => 'app-menu'], false, false);
        
        $this->response($menus, 200);
    }
}
