<!DOCTYPE html>
<html>
<head>
	<?= $this->ometa->home($home) ?>
	<?= $this->theme->file('partial/head') ?>
</head>
<body>
	<?= $this->theme->file('partial/navbar') ?>
	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-content">
						<h1>Heading 1 <small>Secondary text</small></h1>
						<h2>Heading 2 <small>Secondary text</small></h2>
						<h3>Heading 3 <small>Secondary text</small></h3>
						<h4>Heading 4 <small>Secondary text</small></h4>
						<h5>Heading 5 <small>Secondary text</small></h5>
						<h6>Heading 6 <small>Secondary text</small></h6>
						<p class="lead">This is a lead for paragraphs.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?= $this->theme->file('partial/foot') ?>
</body>
</html>