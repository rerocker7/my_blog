<!DOCTYPE html>
<html>
<head>
    <?= $this->ometa->post($post); ?>
    <?= $this->theme->file('partial/head') ?>
</head>
<body>
    <?= $this->theme->file('partial/navbar') ?>
	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="content">
					<h1><?= $post->title ?></h1>
					<p><?= $post->content ?></p>
				</div>
			</div>
		</div>
	</div>

    <?= $this->theme->file('partial/foot') ?>
    <?= $this->meta->footer() ?>
</body>
</html>